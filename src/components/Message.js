import React  from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

const Message = (props) => {
    return(
         <Modal
            show={props.show}
            onHide={props.setShow}
            backdrop="static"
            keyboard={false}
          >
            <Modal.Body>
              {props.msg}
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={props.setShow}>
                Continue
              </Button>
            </Modal.Footer>
          </Modal>
    );
}

export default Message;
