import React , { Component } from 'react';
import Layout from './hoc/Layout';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import TodoAppBuilder from './containers/TodoAppBuilder';
import TodoListBuilder from './containers/TodoListBuilder';


class App extends Component {
    render(){
        return (
            <div className="App">
                <Layout>
                    <TodoAppBuilder />
                    <TodoListBuilder />
                </Layout>
            </div>
        );
    }
}

export default App;
