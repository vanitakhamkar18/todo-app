import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyDWkDgmAi1iY2KhXGmt13KfTXzKNUsRkxU",
  authDomain: "test-vani.firebaseapp.com",
  databaseURL: "https://test-vani.firebaseio.com",
  projectId: "test-vani",
  storageBucket: "test-vani.appspot.com",
  messagingSenderId: "470738033584",
  appId: "1:470738033584:web:0082f0ee4dfd7a7760d475",
  measurementId: "G-XLV3855PJ1"
};

firebase.initializeApp(firebaseConfig);

export default firebase;
